cmake_minimum_required(VERSION 3.8)

project(examples)

link_directories(../lib)
link_directories(../extern/lib)
include_directories(../include)
include_directories(../extern/include)
set(Libraries uv rpc event cjson pthread event_pthreads sigsegv zlog)

add_subdirectory(helloworld)
add_subdirectory(service)
add_subdirectory(service_params)
add_subdirectory(service_api)
add_subdirectory(observer)
add_subdirectory(period)
add_subdirectory(security)


